// MainActivity is used to interact with DBAdatpter, manage SharedPreferences, and validate user input.

package com.example.parrot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import android.view.View;

public class MainActivity extends Activity {

	private DBAdapter dbAdapter = new DBAdapter(this); // instantiates DBAdapter
	SharedPreferences prefs; // used to save and retrieve whick level the user is on
	private int level = 0; // the user's current level
	Button btnSubmit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		prefs = getSharedPreferences("Preferences", 0); // gets the "Preferences" stored preference.
		level = prefs.getInt("level", 1); // sets "level" to the user's current level

		// populates database
		dbAdapter.open();
		dbAdapter.deleteAll();
		dbAdapter.insert(1, "There's no place like home.", "WIZARDOFOZ", 1939);
		dbAdapter.insert(2, "I'll be back.", "TERMINATOR", 1984);
		dbAdapter.insert(3, "My Mama always said, Life was like a box of chocolates; you never know what you're gonna get.", "FORRESTGUMP", 1994);
		dbAdapter.insert(4, "You talkin' to me? You talkin' to me? You talkin' to me? Well, who the hell else are you talkin' to? You talkin' to me? Well, I'm the only one here. Who the f**k do you think you're talkin' to?", "TAXIDRIVER", 1976);
		dbAdapter.insert(5, "Houston, we have a problem.", "APOLLO13", 1995);
		dbAdapter.insert(6, "Last night, Darth Vader came down from Planet Vulcan and told me that if I didn't take Lorraine out, that he'd melt my brain.", "BACKTOFUTURE", 1985);
		dbAdapter.insert(7, "Wax on, wax off.", "KARATEKID", 1984);
		dbAdapter.insert(8, "Buddy the Elf, what's your favorite color?", "ELF", 2003);
		dbAdapter.insert(9, "I see dead people.", "SIXTHSENSE", 1999);
		dbAdapter.insert(10, "You can't handle the truth!", "AFEWGOODMEN", 1992);
		
		// sets the user's level to 1
		if (level == 1 || level == 0) {
			prefs = getSharedPreferences("Preferences", 0);
			Editor editor = prefs.edit();
			editor.putInt("level", 1);
			editor.commit();
			level = prefs.getInt("level", 1);
		}
		
        try {
        	TextView lbl = (TextView)findViewById(R.id.lblQuote);
			Cursor cs = dbAdapter.getQuote(level); // gets a quote and year
			lbl.setText(level+".   "+"'"+cs.getString(0)+"'"); // displays the quote
			lbl = (TextView)findViewById(R.id.lblYear);
			lbl.setText("~ "+cs.getString(1)); // displays year
	 	} catch (Exception e) {
	 		e.printStackTrace();
	 	}
        dbAdapter.close(); // close connection
        
        // makes an on click listener
	    btnSubmit = (Button) findViewById(R.id.btnSubmit);
		btnSubmit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				btnSubmitOnClick(v);
			}
		});
	}
	
	private void btnSubmitOnClick(View view) {
		EditText txtAnswer = (EditText) findViewById(R.id.txtAnswer);
		String userAnswer = txtAnswer.getText().toString().toUpperCase(); // gets the user's answer and converts it to uppercase
		// The next two lines create a richer user experience.
		userAnswer = userAnswer.replace("THE", ""); // removes all "THE" strings from the user's answer.
		userAnswer = userAnswer.replace(" ", ""); // removes all spaces from the user's answer.
		dbAdapter.open(); // open DB connection
		Cursor answer = dbAdapter.getAnswer(level); // retrieves the correct answer from the database.
		if (userAnswer.equals(answer.getString(0))) { // checks if user answer and the real answer match
			Toast.makeText(getBaseContext(), "RIGHT!", Toast.LENGTH_LONG).show();
			level++; // ups the user's level
			Editor editor = prefs.edit();
			editor.putInt("level", level); // ups the user's level on the shared preference
			editor.commit();
			if (level <= 10) {
				finish(); // ends the activity
				startActivity(getIntent()); // restarts the activity
			} else {
				Intent i = new Intent(MainActivity.this, EndActivity.class);
				startActivity(i); // starts EndActivity
				finish(); // ends MainActivity
			}
		} else {
			btnSubmit.setBackgroundResource(R.drawable.button_cancel);
			Toast.makeText(getBaseContext(), "WRONG!", Toast.LENGTH_LONG).show();
		}
		dbAdapter.close();
	}
}
