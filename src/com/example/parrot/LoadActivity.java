package com.example.parrot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class LoadActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load);
		// creates an on click listener
		final Button btnPlay = (Button) findViewById(R.id.btnPlay);
		btnPlay.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				btnPlayOnClick(v);
			}
		});
	}

	private void btnPlayOnClick(View v) {
		SharedPreferences prefs = getSharedPreferences("Preferences", 0); // grabs user's level
		if (prefs.getInt("level", 1) <= 10) {
			Intent i = new Intent(LoadActivity.this, MainActivity.class);
			startActivity(i); // continues to MainActivity
		} else {
			Intent i = new Intent(LoadActivity.this, EndActivity.class);
			startActivity(i); // displays EndActivity
		}
	}
}
