// DBAdapter is used to perform all database interactions through SQLite.

package com.example.parrot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {

	public static final String KEY_ROWID = "_id"; // stores row id
	public static final String KEY_QUOTE = "quote"; // stores quote
	public static final String KEY_ANSWER = "answer"; // stores movie title
	public static final String KEY_YEAR = "year"; // stores year film was released

	private static final String DATABASE_NAME = "Parrot";
	private static final String DATABASE_TABLE = "quotes";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table quotes (_id integer primary key autoincrement, "
			+ "quote text not null, answer text not null, year integer not null);";

	final Context context;
	private SQLiteHelper sqLiteHelper;
	private SQLiteDatabase sqLiteDatabase;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		sqLiteHelper = new SQLiteHelper(context);
	}

	// opens SQLite connection
	public DBAdapter open() throws SQLException { 
		sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		return this;
	}
	
	// closes SQLite connection
	public void close() { 
		sqLiteHelper.close();
	}

	// inserts values into database
	public long insert(int id, String quote, String answer, int year) {
		ContentValues values = new ContentValues();
		values.put(KEY_ROWID, id); // inserts an int starting at 1
		values.put(KEY_QUOTE, quote); // inserts a quote string
		values.put(KEY_ANSWER, answer); // inserts a movie title as a string
		values.put(KEY_YEAR, year); // inserts the year the film was released as an integer
		return sqLiteDatabase.insert(DATABASE_TABLE, null, values);
	}

	public int deleteAll() {
		return sqLiteDatabase.delete(DATABASE_TABLE, null, null);
	}
	
	// retrieves quote and year from database
	public Cursor getQuote(int rowId) throws SQLException {
		Cursor mCursor = sqLiteDatabase.query(true, DATABASE_TABLE,
				new String[] {KEY_QUOTE, KEY_YEAR},
				KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (mCursor != null)
			mCursor.moveToFirst();
		return mCursor;
	}
	
	// retrieves movie title from database
	public Cursor getAnswer(int rowId) throws SQLException {
		Cursor mCursor = sqLiteDatabase.query(true, DATABASE_TABLE,
				new String[] {KEY_ANSWER},
				KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (mCursor != null)
			mCursor.moveToFirst();
		return mCursor;
	}

	public class SQLiteHelper extends SQLiteOpenHelper {
		SQLiteHelper(Context context) {
			super (context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(DATABASE_CREATE); // create the database
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE); // drop the existing table
			onCreate(db); // re-create the table
		}
	}
}